#!/usr/bin/env bash
set -e

sudo docker pull edox93/api-search:version7 |
   grep "Image is up to date" ||
   (echo Already up to date. Exiting... && exit 0)


echo '>>> Get old container id'
CID=$(sudo docker ps --all | grep "api-search" | awk '{print $1}')
echo $CID


echo '>>> Stopping and deleting old container'
if [ "$CID" != "" ];
then
  sudo docker stop $CID
  sudo docker rm $CID
fi


echo '>>> Starting new container'
sudo docker run --name=api-search  -d edox93/api-search:version7

